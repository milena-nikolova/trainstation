package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class Train extends MainStations // II.
{
	Vector<StopStation> stopStations = new Vector<StopStation>(); 	// ������ � ���������� ������ �� �����
	private int maxSeats;
	private int takenSeats;


	public Train() {
		super();
		this.setMaxSeats(50);
		this.setTakenSeats(0);
	}

	public Train(String startStation, String endStation, int distance, String departureTime, String arrivalTime,
			String duriationTime, String trainID, Vector<StopStation> stopStations) {
		super(startStation, endStation, distance, departureTime, arrivalTime, duriationTime, trainID);
		this.stopStations = stopStations;
		this.setMaxSeats(50);
		this.setTakenSeats(0);
	}

	// --- ��������� � ����� �� ������
	public Train(String startStation, String endStation, int distance, String departureTime, String arrivalTime,
			String duriationTime, String trainID) throws Exception {
		super(startStation, endStation, distance, departureTime, arrivalTime, duriationTime, trainID);
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:trainadmin/123456@localhost:1521:ORCL");
		String query = "SELECT * FROM mejdinni_spirki WHERE nomer = N'" + trainID + "'";
		Statement statment = connection.createStatement();
		ResultSet resultSet = statment.executeQuery(query);
		StopStation stop;
		while (resultSet.next()) {
			stop = new StopStation();
			stop.setArrivalTime(resultSet.getString(3));
			stop.setDepartureTime(resultSet.getString(4));
			stop.setDowntime(resultSet.getString(5));
			stop.setTrainID(resultSet.getString(1));
			stop.setStationName(resultSet.getString(2));
			stopStations.add(stop);
		}
		this.setMaxSeats(50);
		this.setTakenSeats(0);
	}

	// Accessor
	public Vector<StopStation> getStops() {
		return stopStations;
	}

	// ������� �� �������� ���� �� �������� ���
	public StopStation searchStopStation(String name) {
		for (StopStation stop : stopStations) {
			if (stop.getStationName().equals(name))
				return stop;
		}
		return null;
	}

	// �������� �������
	public double averageSpeed() {
		double speed, distance, time, minutes, hours;
		String temp;
		temp = this.getDuriationTime().substring(3, 5);
		minutes = Integer.parseInt(temp);
		temp = this.getDuriationTime();
		temp = this.getDuriationTime().substring(0, 2);
		hours = Integer.parseInt(temp);
		minutes /= 60;// ���������� �� �������� � ������
		time = minutes + hours;
		distance = this.getDistance();

		speed = distance / time; // v=s/t
		return speed;
	}

	public int getMaxSeats() {
		return maxSeats;
	}

	public void setMaxSeats(int maxSeats) {
		this.maxSeats = maxSeats;
	}

	public int getTakenSeats() {
		return takenSeats;
	}

	public void setTakenSeats(int takenSeats) {
		this.takenSeats = takenSeats;
	}

};
