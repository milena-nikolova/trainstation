package application;

public class StopStation {
	private String trainID; 				// ����� �� �����
	private String arrivalTime; 			// ��� �� ���������� HH:MM
	private String departureTime;			// ��� �� ��������� HH:MM
	private String downtime; 				// ����������� �� ������� HH:MM
	private String stationName; 			// ��� �� ������

	public StopStation() {
	}

	public StopStation(String trainID, String arrivalTime, String departureTime, String downtime, String stationName) {
		this.trainID = trainID;
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
		this.downtime = downtime;
		this.stationName = stationName;
	}

	// -- Mutators
	public void Print() {
		System.out.println(stationName + " " + departureTime + " " + arrivalTime + " " + trainID);
	}

	public void setTrainID(String trainID) {
		this.trainID = trainID;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public void setDowntime(String downtime) {
		this.downtime = downtime;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	// ---- Accessors
	public String getTrainID() {
		return trainID;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public String getDownTime() {
		return downtime;
	}

	public String getStationName() {
		return stationName;
	}
};