package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class TrainNet 
{
	private Vector<Train> trainNet = new Vector<Train>();
	private int ticketNumber;

	// ��������� �� ��������� ����� �� ������
	public TrainNet() throws Exception {
		Train train;
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:trainadmin/123456@localhost:1521:ORCL");
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM vlakove");
		while (rs.next()) {
			train = new Train(rs.getString(2), rs.getString(3), rs.getInt(7), rs.getString(4), rs.getString(5),
					rs.getString(6), rs.getString(1));
			trainNet.add(train);
		}
		setTicketNumber(0);
	}

	// ������� �� ������ � �������� ����� �� ������
	public static Vector<String> getUniqueStops() throws Exception {
		Vector<String> name = new Vector<String>();
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:trainadmin/123456@localhost:1521:ORCL");
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT distinct nachalna_gara FROM vlakove order by nachalna_gara asc");
		while (rs.next()) {
			name.add(rs.getString(1));
		}
		rs = stmt.executeQuery("SELECT distinct mejdinna_gara FROM mejdinni_spirki order by mejdinna_gara asc");
		while (rs.next()) {
			name.add(rs.getString(1));
		}
		return name;
	}

	// ������� �� ������� ���� �� �������� ���
	public Vector<Train> searchStartStation(String stationName, Vector<Train> trainNet) {
		Vector<Train> trains = new Vector<Train>();
		for (Train train : trainNet) {
			if (train.getStartStation().equals(stationName))
				trains.add(train);
		}
		return trains;
	}

	// ������� �� ������ ���� �� �������� ���
	public Vector<Train> searchEndStation(String stationName, Vector<Train> trainNet) {
		Vector<Train> trains = new Vector<Train>();
		for (Train train : trainNet) {
			if (train.getEndStation().equals(stationName))
				trains.add(train);
		}
		return trains;
	}

	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	public String getTrainLocation(String trainID) {
		String currentStation = "";
		Train obj = getTrainByID(trainID);
		java.util.Date date = new java.util.Date();

		String current = date.getHours() + ":" + date.getMinutes();
		String arrival = obj.getArrivalTime();
		String departure = obj.getDepartureTime();

		boolean flag = false;

		if (compareHours(current, departure) > 0 && compareHours(current, arrival) < 0) {

			String arrivalAtStation;
			String departureFromStation;
			Vector<StopStation> trains = obj.getStops();

			Collections.sort(trains, new Comparator() {
				public int compare(Object a, Object b) {
					return (new String(((StopStation) a).getDepartureTime()))
							.compareTo(new String(((StopStation) b).getDepartureTime()));
				}
			});

			for (StopStation i : trains) {
				if (flag) {
					currentStation += i.getStationName();
					flag = false;
				}
				arrivalAtStation = i.getArrivalTime();
				departureFromStation = i.getDepartureTime();

				if (compareHours(current, arrivalAtStation) >= 0 && compareHours(current, departureFromStation) <= 0) {
					currentStation = i.getStationName();
				} else if (compareHours(current, departureFromStation) > 0) {
					currentStation = "������ �� " + i.getStationName() + " ��� ";
					flag = true;
				}
			}
			if (flag) {
				currentStation += obj.getEndStation();
				flag = false;
			}

		} else if (compareHours(current, arrival) >= 0) {
			currentStation = obj.getEndStation();
		} else if (compareHours(current, departure) <= 0) {
			currentStation = obj.getStartStation();
		}
		return currentStation;
	}

	// ������� �� �������� ���� �� �������� ��� � �������� ���� �� ������� ��
	// ��� ���� ����� ������
	public Vector<Train> searchStopStation(String stationName, Vector<Train> trainNet, String startStation) {
		Vector<Train> trains = new Vector<Train>();
		for (Train train : trainNet) {
			for (StopStation station : train.getStops()) {
				if (station.getStationName().equals(stationName)
						&& compareHours(train.searchStopStation(startStation).getDepartureTime(),
								train.searchStopStation(stationName).getArrivalTime()) == -1) {
					trains.add(train);
					break;
				}
			}
		}
		return trains;
	}

	// ������� �� �������� ���� �� �������� ���
	public Vector<Train> searchStopStation(String stationName, Vector<Train> trainNet) {
		Vector<Train> trains = new Vector<Train>();
		for (Train train : trainNet) {
			for (StopStation station : train.getStops()) {
				if (station.getStationName().equals(stationName)) {
					trains.add(train);
					break;
				}
			}
		}
		return trains;
	}

	// ���������� �� ������
	@SuppressWarnings("deprecation")
	public static int compareHours(String time, String time2) {
		String obj = "2015-12-12 " + time + ":00.0";
		String obj2 = "2015-12-12 " + time2 + ":00.0";
		Timestamp timestamp = Timestamp.valueOf(obj);
		Timestamp timestamp2 = Timestamp.valueOf(obj2);
		int minus = timestamp.getHours() - timestamp2.getHours();
		if ((timestamp.compareTo(timestamp2) == 1) && (minus > 7)) {
			timestamp2.setDate(13);
		}
		if (timestamp.compareTo(timestamp2) == 1)
			return 1;
		else if (timestamp.compareTo(timestamp2) == 0)
			return 0;
		else
			return -1;
	}

	// ������ �� ������� �� �������� ������� � ������ ������
	public Vector<MainStations> searchRoute(String startStation, String endStation) {
		Vector<MainStations> trains = new Vector<MainStations>();
		Vector<Train> startTrains = this.searchStartStation(startStation, trainNet);
		Vector<Train> endTrains;
		Vector<Train> stopTrains = this.searchStopStation(startStation, trainNet);
		MainStations obj;
		if (startTrains.isEmpty() == false) {
			endTrains = this.searchEndStation(endStation, startTrains);
			stopTrains = this.searchStopStation(endStation, startTrains);
			if (endTrains.isEmpty() == false)
				for (Train train : endTrains) {
					obj = new MainStations(startStation, endStation, 0, train.getDepartureTime(),
							train.getArrivalTime(), calculateTime(train.getDepartureTime(),
									train.getArrivalTime()),
							train.getTrainID());
					trains.add(obj);
				}
			if (stopTrains.isEmpty() == false)
				for (Train train : stopTrains) {
					obj = new MainStations(startStation, endStation, 0, train.getDepartureTime(),
							train.searchStopStation(endStation).getArrivalTime(),
							calculateTime(train.getDepartureTime(),
									train.searchStopStation(endStation).getArrivalTime()), train.getTrainID());
					trains.add(obj);
				}
		}
		stopTrains = this.searchStopStation(startStation, trainNet);
		if (stopTrains.isEmpty() == false) {
			endTrains = this.searchEndStation(endStation, stopTrains);
			stopTrains = this.searchStopStation(endStation, stopTrains, startStation);
			if (endTrains.isEmpty() == false)
				for (Train train : endTrains) {
					obj = new MainStations(startStation, endStation, 0,
							train.searchStopStation(startStation).getDepartureTime(), train.getArrivalTime(),
							calculateTime(train.searchStopStation(startStation).getDepartureTime(),
									train.getArrivalTime()),
							train.getTrainID());
					trains.add(obj);
				}
			if (stopTrains.isEmpty() == false)
				for (Train train : stopTrains) {
					obj = new MainStations(startStation, endStation, 0,
							train.searchStopStation(startStation).getDepartureTime(),
							train.searchStopStation(endStation).getArrivalTime(),
							calculateTime(train.searchStopStation(startStation).getDepartureTime(),
									train.searchStopStation(endStation).getArrivalTime()),
							train.getTrainID());
					trains.add(obj);
				}

		}
		return trains;
	}

	// ������� �� ���� �� �����
	public Train getTrainByID(String id) {
		for (Train i : trainNet) {
			if (i.getTrainID().equals(id)) {
				return i;
			}
		}
		return null;
	}

	// ������� �� ������� �� ������
	public Vector<StopStation> getTrainByStation(String station) {
		Vector<StopStation> trains = new Vector<StopStation>();
		for (Train i : trainNet) {
			if (i.getStartStation().equals(station))
				trains.addElement(
						new StopStation(i.getTrainID(), "--:--", i.getDepartureTime(), "0", i.getStartStation()));
			else if (i.getEndStation().equals(station))
				trains.addElement(new StopStation(i.getTrainID(), i.getArrivalTime(), "--:--", "0", i.getEndStation()));
			else if (i.searchStopStation(station) != null) {
				trains.add(i.searchStopStation(station));
			}
		}
		return trains;
	}

	// Accessor
	public Vector<Train> getVector() {
		return trainNet;
	}

	public void makeReservation(boolean flag, String trainID, String startStation, String endStation) {
		Train a = getTrainByID(trainID);
		
		try {
			FileReader fr = new FileReader("ticketNumbers.txt");
			BufferedReader br = new BufferedReader(fr);
			int ticket = Integer.parseInt(br.readLine());
			setTicketNumber(ticket);
			br.close();
			fr.close();
		} catch (NumberFormatException | IOException e) {
			System.err.println("can't read file");
		}

		try {
			FileWriter fw = new FileWriter("ticket.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			String content;

			if (a.getMaxSeats() == a.getTakenSeats())
				bw.write("������ � �����");
			else if (calculatePrice(trainID, startStation, endStation) == 0) {
				bw.write("������������� �������");
			} else if (!flag && a.getMaxSeats() > a.getTakenSeats()) {
				a.setTakenSeats(a.getTakenSeats() + 1);
				content = "\n������� ����: " + startStation + "\n";
				bw.write(content);
				content = "������ ����: " + endStation + "\n";
				bw.write(content);
				content = "����� ����� #" + getTicketNumber() + "\n";
				bw.write(content);
				setTicketNumber(getTicketNumber() + 1);
				content = "���� �� ������: " + calculatePrice(trainID, startStation, endStation) + "��.\n";
				bw.write(content);
				bw.close();
			} else if (flag && a.getMaxSeats() > a.getTakenSeats() + 2) {
				a.setTakenSeats(a.getTakenSeats() + 2);
				content = "\n����� �� ������� ����� #" + getTicketNumber() + "\n";
				bw.write(content);
				content = "������� ����: " + startStation + "\n";
				bw.write(content);
				content = "������ ����: " + endStation + "\n";
				bw.write(content);
				setTicketNumber(getTicketNumber() + 1);
				content = "\n����� � ������� ����� #" + getTicketNumber() + "\n";
				bw.write(content);
				content = "������� ����: " + endStation + "\n";
				bw.write(content);
				content = "������ ����: " + startStation + "\n";
				bw.write(content);
				setTicketNumber(getTicketNumber() + 1);
				content = "\n������� ����: " + calculatePrice(trainID, startStation, endStation) * 2 + "��.\n";
				bw.write(content);
				bw.close();
			} else
				bw.write("������������� �������");

		} catch (IOException e) {
			System.err.println("can't write ticket");
		}

		try {
			FileWriter fw = new FileWriter("ticketNumbers.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			String ticket = "" + getTicketNumber();
			bw.write(ticket);
			bw.close();
			fw.close();

		} catch (IOException e) {
			System.err.println("can't write to file");
		}
	}

	// price = time in minutes * 0.07??
	public double calculatePrice(String trainID, String startStation, String endStation) {
		double price = 0;
		String startTime = "";
		String endTime = "";
		String durationTime = "";
		Train a = getTrainByID(trainID);

		if (a.getStartStation().equals(startStation)) {
			startTime = a.getDepartureTime();
			if (a.getEndStation().equals(endStation)) {
				endTime = a.getArrivalTime();
			} else if (a.searchStopStation(endStation) != null) {
				endTime = a.searchStopStation(endStation).getArrivalTime();
			}
		} else if (a.searchStopStation(startStation) != null) {
			startTime = a.searchStopStation(startStation).getDepartureTime();
			if (a.getEndStation().equals(endStation)) {
				endTime = a.getArrivalTime();
			} else if (a.searchStopStation(endStation) != null) {
				endTime = a.searchStopStation(endStation).getArrivalTime();
			}
		} else
			return 0;
		


		durationTime = calculateTime(startTime, endTime);
		if (durationTime.length() > 0) {
			Integer time = Integer.parseInt(durationTime.substring(0, durationTime.indexOf(':'))) * 60 + Integer
					.parseInt(durationTime.substring(durationTime.indexOf(':') + 1, durationTime.length() - 1));
			price = time * 0.04;
			return price;
		} else
			return 0;
	}

	public String calculateTime(String time1, String time2) {

		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		java.util.Date date1 = new java.util.Date();
		java.util.Date date2 = new java.util.Date();

		try {
			date1 = format.parse(time1);
			date2 = format.parse(time2);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		long difference = 0;

		if (date1.before(date2)) {
			difference = date2.getTime() - date1.getTime();
		} else {
			try {
				difference = (format.parse("24:00").getTime() - date1.getTime())
						+ (date2.getTime() - format.parse("00:00").getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		String time = "";
		if ((difference / (60 * 60 * 1000) % 24)<10)
			time += "0" ;
		time+= difference / (60 * 60 * 1000) % 24 + ":";
		
		if ((difference / (60 * 1000) % 60)<10) 
			time+="0";
		time += difference / (60 * 1000) % 60;
		return time;
	}

	public int getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(int ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public static void main(String args[]) throws Exception {
		TrainNet o = new TrainNet();
		Vector<MainStations> trains = o.searchRoute("���������", "�����");
		System.out.println(o.getTrainLocation("��2627"));
		//o.getTrainByID("��2627").setTakenSeats(50);
		o.makeReservation(true, "��2627", "���������", "�����");
		o.makeReservation(false, "��2627", "���������", "�����");
		System.out.println("Taken seats: " + o.getTrainByID("��2627").getTakenSeats());

		for (MainStations i : trains) {
			i.Print();
		}
	}
};