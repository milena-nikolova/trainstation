package application;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.util.Vector;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Main extends Application {

	private Stage primaryStage;
	private Scene scene;
	private ImageView logoView;
	private BorderPane root;
	private double xOffset;
	private double yOffset;
	private ObservableList<StopStation> trainList = FXCollections.observableArrayList();
	private ObservableList<String> uniqueStops = FXCollections.observableArrayList();
	private ObservableList<MainStations> trainData = FXCollections.observableArrayList();

	@Override
	public void start(Stage primaryStage) {

		try {
			uniqueStops = FXCollections.observableList(TrainNet.getUniqueStops());
		} catch (Exception e1) {
			System.err.println("Can't connect to database");
		}

		root = new BorderPane();
		root.setTop(getLogo());
		root.setBottom(getFooter());
		root.setLeft(getMenuPanel());
		root.setCenter(getHomePanel());
		root.setId("border");

		scene = new Scene(root, 820, 620, Color.TRANSPARENT);
		scene.getStylesheets().add("application/application.css");

		scene.setOnMousePressed(event -> {
			xOffset = primaryStage.getX() - event.getScreenX();
			yOffset = primaryStage.getY() - event.getScreenY();
		});
		scene.setOnMouseDragged(event -> {
			primaryStage.setX(event.getScreenX() + xOffset);
			primaryStage.setY(event.getScreenY() + yOffset);
		});

		primaryStage.getIcons().add(new Image("resources/images/trainIcon.png"));
		primaryStage.initStyle(StageStyle.TRANSPARENT);
		primaryStage.setTitle("Train Station");
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);

		primaryStage.getScene().addEventFilter(KeyEvent.KEY_PRESSED, (evt) -> {
			if (evt.getCode() == KeyCode.F1) {
				getHelpWindow();
			}
		});
		primaryStage.getScene().addEventFilter(KeyEvent.KEY_PRESSED, (evt) -> {
			if (evt.getCode() == KeyCode.ESCAPE) {
				primaryStage.close();
			}
		});
		primaryStage.getScene().addEventFilter(KeyEvent.KEY_PRESSED, (evt) -> {
			if (evt.getCode() == KeyCode.M) {
				primaryStage.setIconified(true);
			}
		});

		primaryStage.show();
	}

	private StackPane getLogo() {
		StackPane logo = new StackPane();
		logo.setPadding(new Insets(20, 20, 10, 10));
		logo.setAlignment(Pos.BASELINE_RIGHT);

		Image logoPicture = new Image("resources/images/trainStationLogo.png");
		logoView = new ImageView();
		logoView.setImage(logoPicture);
		logoView.setFitWidth(550);
		logoView.setPreserveRatio(true);
		logoView.setSmooth(true);
		logoView.setCache(true);

		logo.getChildren().add(logoView);
		logo.setPrefSize(800, 100);

		return logo;
	}

	private VBox getMenuPanel() {
		VBox menuPanel = new VBox(20);
		menuPanel.setPadding(new Insets(20, 20, 20, 40));

		Button btnHome = new Button("������");
		Button btnScedule = new Button("����������");
		Button btnInformation = new Button("����������");
		Button btnReserve = new Button("����������� �� �����");

		btnHome.setOnAction(e -> root.setCenter(getHomePanel()));
		btnScedule.setOnAction(e -> root.setCenter(getScedulePanel()));
		btnInformation.setOnAction(e -> root.setCenter(getInformationPanel()));
		btnReserve.setOnAction(e -> root.setCenter(getReservationPanel()));

		btnScedule.setMaxWidth(Double.MAX_VALUE);
		btnInformation.setMaxWidth(Double.MAX_VALUE);
		btnReserve.setMaxWidth(Double.MAX_VALUE);
		btnHome.setMaxWidth(Double.MAX_VALUE);

		menuPanel.getChildren().addAll(btnHome, btnScedule, btnInformation, btnReserve);
		return menuPanel;
	}

	private VBox getFooter() {
		VBox footer = new VBox(2);
		footer.setPadding(new Insets(10, 10, 20, 10));
		footer.getChildren().add(new Label("ESC - ����� �� ����������, F1 - ������� ��������, M - ������������"));
		footer.getChildren().add(new Label("������ ������ ���2 �� ������� ����������� � ������ ��������"));
		footer.setAlignment(Pos.BASELINE_CENTER);

		return footer;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private GridPane getHomePanel() {
		GridPane homePanel = new GridPane();
		homePanel.setPadding(new Insets(20, 20, 20, 20));
		homePanel.setVgap(20);
		homePanel.setHgap(20);

		ObservableList<String> themes = FXCollections.observableArrayList(
				"Default", 
				"Black&White", 
				"Sepia", 
				"Sky Blue",
				"Nature");

		Label lblTheme = new Label("Theme color:");
		final ComboBox cbTheme = new ComboBox(themes);
		cbTheme.setPromptText("choose theme");

		cbTheme.setOnAction(e -> changeTheme(cbTheme.getValue()));

		homePanel.add(lblTheme, 0, 0);
		homePanel.add(cbTheme, 1, 0);
		return homePanel;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private GridPane getScedulePanel() {

		GridPane scedulePanel = new GridPane();
		scedulePanel.setPadding(new Insets(20, 20, 50, 100));
		scedulePanel.setVgap(20);
		scedulePanel.setHgap(20);

		// scedulePanel.setGridLinesVisible(true);
		Label lblStartStation = new Label("������� ����:");
		Label lblEndStation = new Label("������ ����:");
		Label lblDate = new Label("����:");

		final ComboBox cbStartStation = new ComboBox(uniqueStops);
		cbStartStation.setPromptText("������� ����");

		final ComboBox cbEndStation = new ComboBox(uniqueStops);
		cbEndStation.setPromptText("������ ����");

		DatePicker datePicker = new DatePicker();

		cbEndStation.setMaxWidth(Double.MAX_VALUE);
		cbStartStation.setMaxWidth(Double.MAX_VALUE);
		datePicker.setMaxWidth(Double.MAX_VALUE);

		HBox buttons = new HBox(20);

		Button btnSearch = new Button("�����");
		Button btnClear = new Button("�������");

		btnSearch.setPrefWidth(100);
		btnClear.setPrefWidth(100);

		buttons.getChildren().addAll(btnSearch, btnClear);

		btnSearch.disableProperty().bind(datePicker.valueProperty().isNull().or(cbStartStation.valueProperty().isNull())
				.or(cbEndStation.valueProperty().isNull()));

		btnSearch.setOnAction(e -> createTable(cbStartStation.getValue().toString(), cbEndStation.getValue().toString(),
				datePicker.getValue()));

		btnClear.setOnAction(e -> {
			cbStartStation.setValue(null);
			cbEndStation.setValue(null);
			datePicker.setValue(null);
		});

		scedulePanel.add(lblStartStation, 0, 0);
		scedulePanel.add(lblEndStation, 0, 1);
		scedulePanel.add(cbStartStation, 1, 0);
		scedulePanel.add(cbEndStation, 1, 1);
		scedulePanel.add(lblDate, 0, 2);
		scedulePanel.add(datePicker, 1, 2);
		scedulePanel.add(buttons, 1, 3);

		return scedulePanel;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private VBox getInformationPanel() {

		VBox informationPanel = new VBox(20);
		informationPanel.setPadding(new Insets(20, 20, 50, 100));

		Label lblInfo = new Label("���������� �� ������e � ����.");
		GridPane searchInfo = new GridPane();

		searchInfo.setVgap(20);
		searchInfo.setHgap(20);

		Label lblTrainNumber = new Label("����� �� ����:");
		TextField tfTrainNumber = new TextField();
		tfTrainNumber.setMaxWidth(Double.MAX_VALUE);
		tfTrainNumber.setPromptText("����� �� ����");
		Button btnSearchTrain = new Button("�����");
		btnSearchTrain.setOnAction(e -> tableTrainInfo(tfTrainNumber.getText()));
		btnSearchTrain.disableProperty().bind(Bindings.isEmpty(tfTrainNumber.textProperty()));

		Label lblStation = new Label("��� �� ����:");
		ComboBox cbStation = new ComboBox(uniqueStops);
		cbStation.setMaxWidth(Double.MAX_VALUE);
		cbStation.setPromptText("��� �� ����");
		Button btnSearchStation = new Button("�����");
		btnSearchStation.setOnAction(e -> tableStationInfo(cbStation.getValue().toString()));
		btnSearchStation.disableProperty().bind(cbStation.valueProperty().isNull());

		searchInfo.add(lblTrainNumber, 0, 0);
		searchInfo.add(tfTrainNumber, 1, 0);
		searchInfo.add(btnSearchTrain, 2, 0);

		searchInfo.add(lblStation, 0, 1);
		searchInfo.add(cbStation, 1, 1);
		searchInfo.add(btnSearchStation, 2, 1);

		informationPanel.getChildren().addAll(lblInfo, searchInfo);

		return informationPanel;
	}

	private TabPane getReservationPanel() {

		TabPane reservationPanel = new TabPane();
		reservationPanel.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		reservationPanel.setPadding(new Insets(20, 20, 20, 20));

		Tab oneWayTickets = new Tab("�����������");
		oneWayTickets.setContent(createTab(false));

		Tab twoWayTickets = new Tab("����������");
		twoWayTickets.setContent(createTab(true));

		reservationPanel.getTabs().addAll(oneWayTickets, twoWayTickets);

		return reservationPanel;
	}

	private void changeTheme(Object value) {
		scene.getStylesheets().clear();
		Image logoPicture;

		if (value.toString() == "Black&White") {
			logoPicture = new Image("resources/images/logo-b&w.png");
			logoView.setImage(logoPicture);
			scene.getStylesheets().add("resources/css/dark.css");
		} else if (value.toString() == "Default") {
			logoPicture = new Image("resources/images/logo-default.png");
			logoView.setImage(logoPicture);
			scene.getStylesheets().add("application/application.css");
		} else if (value.toString() == "Sepia") {
			logoPicture = new Image("resources/images/logo-sepia.png");
			logoView.setImage(logoPicture);
			scene.getStylesheets().add("resources/css/sepia.css");
		} else if (value.toString() == "Sky Blue") {
			logoPicture = new Image("resources/images/logo-blue.png");
			logoView.setImage(logoPicture);
			scene.getStylesheets().add("resources/css/blue.css");
		} else if (value.toString() == "Nature") {
			logoPicture = new Image("resources/images/logo-green.png");
			logoView.setImage(logoPicture);
			scene.getStylesheets().add("resources/css/nature.css");
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private GridPane createTab(boolean value) {

		GridPane reservationTab = new GridPane();
		reservationTab.setId("tabarea");
		reservationTab.setVgap(20);
		reservationTab.setHgap(40);
		reservationTab.setPadding(new Insets(10, 10, 10, 20));

		Label lblTrain = new Label("����� �� ����:");
		TextField tfTrainNumber = new TextField();
		tfTrainNumber.setPromptText("����� �� ����");

		Label lblStartStation = new Label("������� ����:");
		ComboBox cbStartStation = new ComboBox(uniqueStops);
		cbStartStation.setPromptText("������� ����");

		Label lblEndStation = new Label("������ ����:");
		ComboBox cbEndStation = new ComboBox(uniqueStops);
		cbEndStation.setPromptText("������ ����");

		tfTrainNumber.setMaxWidth(Double.MAX_VALUE);
		cbEndStation.setMaxWidth(Double.MAX_VALUE);
		cbStartStation.setMaxWidth(Double.MAX_VALUE);

		Label lblDate = new Label("���� �� ����������:");
		final DatePicker datePicker = new DatePicker();
		datePicker.setMaxWidth(Double.MAX_VALUE);

		Label lblReturnDate = new Label("���� �� �������:");
		final DatePicker returnDate = new DatePicker();
		returnDate.setMaxWidth(Double.MAX_VALUE);

		HBox buttons = new HBox(20);

		Button btnReserve = new Button("�������");
		Button btnClear = new Button("�������");

		btnReserve.setPrefWidth(100);
		btnClear.setPrefWidth(100);

		buttons.getChildren().addAll(btnReserve, btnClear);

		btnReserve.setOnAction(null);
		btnClear.setOnAction(e -> {
			tfTrainNumber.setText(null);
			cbStartStation.setValue(null);
			cbEndStation.setValue(null);
			datePicker.setValue(null);
			returnDate.setValue(null);
		});
		if (value) {
			btnReserve.disableProperty()
					.bind(datePicker.valueProperty().isNull().or(returnDate.valueProperty().isNull())
							.or(cbStartStation.valueProperty().isNull()).or(cbEndStation.valueProperty().isNull())
							.or(Bindings.isEmpty(tfTrainNumber.textProperty())));
		} else
			btnReserve.disableProperty()
					.bind(datePicker.valueProperty().isNull().or(cbStartStation.valueProperty().isNull())
							.or(cbEndStation.valueProperty().isNull())
							.or(Bindings.isEmpty(tfTrainNumber.textProperty())));
		TrainNet obj;
		try {
			obj = new TrainNet();
			btnReserve.setOnAction(e -> {
				if (cbStartStation.getValue().toString().equals(cbEndStation.getValue().toString())) {
					Alert dialog = new Alert(Alert.AlertType.ERROR);
					dialog.setHeaderText("��������� � �������� ������ �� �������");
					dialog.setContentText("���� �������� ������!");
					((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons()
							.add(new Image("resources/images/trainIcon.png"));
					dialog.showAndWait();
				} else {
					obj.makeReservation(value, tfTrainNumber.getText(), cbStartStation.getValue().toString(),
							cbEndStation.getValue().toString());
					getTicketWindow(value, datePicker.getValue(), returnDate.getValue());
				}
			});
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		btnClear.setOnAction(e -> {
			tfTrainNumber.setText("");
			cbStartStation.setValue(null);
			cbEndStation.setValue(null);
			datePicker.setValue(null);
			if (value)
				returnDate.setValue(null);
		});

		reservationTab.add(lblTrain, 0, 0);
		reservationTab.add(tfTrainNumber, 1, 0);
		reservationTab.add(lblStartStation, 0, 1);
		reservationTab.add(cbStartStation, 1, 1);
		reservationTab.add(lblEndStation, 0, 2);
		reservationTab.add(cbEndStation, 1, 2);
		reservationTab.add(lblDate, 0, 3);
		reservationTab.add(datePicker, 1, 3);

		if (value) {
			reservationTab.add(lblReturnDate, 0, 4);
			reservationTab.add(returnDate, 1, 4);
			reservationTab.add(buttons, 1, 5);
		} else
			reservationTab.add(buttons, 1, 4);

		return reservationTab;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void createTable(String startStation, String endStation, LocalDate date) {

		trainData.clear();
		try {
			TrainNet o = new TrainNet();
			trainData.addAll(o.searchRoute(startStation, endStation));
		} catch (Exception e1) {
			System.err.println("Empty");
		}

		if (!trainData.isEmpty()) {

			final Stage sceduleInfo = new Stage();
			sceduleInfo.getIcons().add(new Image("resources/images/trainIcon.png"));
			sceduleInfo.initModality(Modality.APPLICATION_MODAL);
			sceduleInfo.initOwner(primaryStage);
			sceduleInfo.setTitle("����������");

			TableView<MainStations> trainTable = new TableView<MainStations>();

			HBox info = new HBox(5);
			info.setAlignment(Pos.BASELINE_CENTER);

			final Label lblStartStation = new Label("�� ����: " + startStation);
			final Label lblEndStation = new Label("��� ����: " + endStation);
			final Label lblDate = new Label("����: " + date);

			info.getChildren().addAll(lblStartStation, lblEndStation, lblDate);

			TableColumn<MainStations, String> trainID = new TableColumn<MainStations, String>("����� �� ����");
			trainID.setCellValueFactory(new PropertyValueFactory("trainID"));
			trainID.setPrefWidth(100);

			TableColumn<MainStations, String> departureTime = new TableColumn<MainStations, String>("��������");
			departureTime.setCellValueFactory(new PropertyValueFactory("departureTime"));
			departureTime.setPrefWidth(100);

			TableColumn<MainStations, String> arrivalTime = new TableColumn<MainStations, String>("��������");
			arrivalTime.setCellValueFactory(new PropertyValueFactory("arrivalTime"));
			arrivalTime.setPrefWidth(100);

			TableColumn<MainStations, String> durationTime = new TableColumn<MainStations, String>("�����������");
			durationTime.setCellValueFactory(new PropertyValueFactory("duriationTime"));
			durationTime.setPrefWidth(100);

			trainTable.getColumns().setAll(trainID, departureTime, arrivalTime, durationTime);
			trainTable.setItems(trainData);

			final VBox vbox = new VBox();
			vbox.setSpacing(10);
			vbox.setAlignment(Pos.CENTER);
			vbox.setPadding(new Insets(10, 10, 10, 10));
			vbox.getChildren().addAll(info, trainTable);

			Scene informationScene = new Scene(vbox);
			sceduleInfo.setScene(informationScene);
			sceduleInfo.show();

		} else {
			Alert dialog = new Alert(Alert.AlertType.ERROR);
			dialog.setHeaderText("������������� �������");
			dialog.setContentText("���� �������� ������!");
			((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons()
					.add(new Image("resources/images/trainIcon.png"));
			dialog.showAndWait();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void tableTrainInfo(String trainID) {

		trainList.clear();
		TrainNet o = null;
		try {
			o = new TrainNet();
			trainList.add(new StopStation(o.getTrainByID(trainID).getTrainID(), "--:--",
					o.getTrainByID(trainID).getDepartureTime(), "0", o.getTrainByID(trainID).getStartStation()));

			Vector<StopStation> trains = o.getTrainByID(trainID).getStops();
			Collections.sort(trains, new Comparator() {
				public int compare(Object a, Object b) {
					return (new String(((StopStation) a).getDepartureTime()))
							.compareTo(new String(((StopStation) b).getDepartureTime()));
				}
			});
			trainList.addAll(FXCollections.observableArrayList(trains));

			trainList.add(new StopStation(o.getTrainByID(trainID).getTrainID(),
					o.getTrainByID(trainID).getArrivalTime(), "--:--", "0", o.getTrainByID(trainID).getEndStation()));

		} catch (Exception e1) {
			Alert dialog = new Alert(Alert.AlertType.ERROR);
			dialog.setHeaderText("������������� ����");
			dialog.setContentText("���� �������� ������!");
			((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons()
					.add(new Image("resources/images/trainIcon.png"));
			dialog.showAndWait();
		}

		if (!trainList.isEmpty()) {
			final Stage trainInfo = new Stage();
			trainInfo.getIcons().add(new Image("resources/images/trainIcon.png"));
			trainInfo.initModality(Modality.APPLICATION_MODAL);
			trainInfo.initOwner(primaryStage);
			trainInfo.setTitle("����������");

			TableView<StopStation> trainInfoTable = new TableView<StopStation>();

			final Label lblTrainID = new Label("���� ����� " + trainID);
			final Label lblTrainLocation = new Label("��������������: " + o.getTrainLocation(trainID));

			TableColumn<StopStation, String> stationName = new TableColumn<StopStation, String>("����");
			stationName.setCellValueFactory(new PropertyValueFactory("stationName"));
			stationName.setPrefWidth(200);

			TableColumn<StopStation, String> arrivalTime = new TableColumn<StopStation, String>("��������");
			arrivalTime.setCellValueFactory(new PropertyValueFactory("arrivalTime"));
			arrivalTime.setPrefWidth(100);

			TableColumn<StopStation, String> departureTime = new TableColumn<StopStation, String>("��������");
			departureTime.setCellValueFactory(new PropertyValueFactory("departureTime"));
			departureTime.setPrefWidth(100);

			trainInfoTable.getColumns().setAll(stationName, arrivalTime, departureTime);

			trainInfoTable.setItems(trainList);

			final VBox vbox = new VBox();
			vbox.setSpacing(5);
			vbox.setAlignment(Pos.CENTER);
			vbox.setPadding(new Insets(10, 10, 10, 10));
			vbox.getChildren().addAll(lblTrainID, lblTrainLocation, trainInfoTable);

			Scene informationScene = new Scene(vbox);
			trainInfo.setScene(informationScene);
			trainInfo.show();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void tableStationInfo(String station) {

		trainList.clear();

		try {
			TrainNet o = new TrainNet();
			Vector<StopStation> trainData = o.getTrainByStation(station);
			trainList.addAll(trainData);

		} catch (Exception e1) {
			System.err.println(e1);
		}

		if (!trainList.isEmpty()) {
			final Stage trainInfo = new Stage();
			trainInfo.getIcons().add(new Image("resources/images/trainIcon.png"));
			trainInfo.initModality(Modality.APPLICATION_MODAL);
			trainInfo.initOwner(primaryStage);
			trainInfo.setTitle("����������");

			TableView<StopStation> trainInfoTable = new TableView<StopStation>();

			final Label lblTrainID = new Label("���� " + station);

			// trainInfoTable.setEditable();

			TableColumn<StopStation, String> trainID = new TableColumn<StopStation, String>("����");
			trainID.setCellValueFactory(new PropertyValueFactory("trainID"));
			trainID.setPrefWidth(200);

			TableColumn<StopStation, String> departureTime = new TableColumn<StopStation, String>("��������");
			departureTime.setCellValueFactory(new PropertyValueFactory("departureTime"));
			departureTime.setPrefWidth(100);

			TableColumn<StopStation, String> arrivalTime = new TableColumn<StopStation, String>("��������");
			arrivalTime.setCellValueFactory(new PropertyValueFactory("arrivalTime"));
			arrivalTime.setPrefWidth(100);

			trainInfoTable.getColumns().setAll(trainID, arrivalTime, departureTime);

			trainInfoTable.setItems(trainList);

			final VBox vbox = new VBox();
			vbox.setSpacing(10);
			vbox.setAlignment(Pos.CENTER);
			vbox.setPadding(new Insets(10, 10, 10, 10));
			vbox.getChildren().addAll(lblTrainID, trainInfoTable);

			Scene informationScene = new Scene(vbox);
			trainInfo.setScene(informationScene);
			trainInfo.show();
		} else {
			Alert dialog = new Alert(Alert.AlertType.ERROR);
			dialog.setHeaderText("���� ������� �� ���������� ����");
			dialog.setContentText("���� �������� ������!");
			((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons()
					.add(new Image("resources/images/trainIcon.png"));
			dialog.showAndWait();
		}
	}

	@SuppressWarnings("resource")
	private void getTicketWindow(boolean flag, LocalDate date1, LocalDate date2) {

		final Stage helpWindow = new Stage();
		helpWindow.getIcons().add(new Image("resources/images/trainIcon.png"));
		helpWindow.initModality(Modality.APPLICATION_MODAL);
		helpWindow.initOwner(primaryStage);
		helpWindow.setTitle("������");

		final TextArea ticket = new TextArea();
		ticket.setFocusTraversable(false);
		ticket.setEditable(false);
		ticket.setWrapText(true);
		ticket.setPrefHeight(450);

		try {
			Scanner s = new Scanner(new File("ticket.txt")).useDelimiter(" ");
			if (s.hasNext()) {
				if (flag) {
					ticket.appendText("���� �� ����������: " + date1.getYear() + " " + date1.getMonth() + " "
							+ date1.getDayOfMonth() + "\n");
					ticket.appendText("���� �� �������: " + date2.getYear() + " " + date2.getMonth() + " "
							+ date2.getDayOfMonth() + "\n");
				} else
					ticket.appendText("���� �� ����������: " + date1.getYear() + " " + date1.getMonth() + " "
							+ date1.getDayOfMonth() + "\n");
				while (s.hasNext()) {
					if (s.hasNextInt()) {
						ticket.appendText(s.nextInt() + " ");
					} else {
						ticket.appendText(s.next() + " ");
					}
				}
			} else {
				Alert dialog = new Alert(Alert.AlertType.ERROR);
				dialog.setHeaderText("������������� �������");
				dialog.setContentText("���� �������� ������!");
				((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons()
						.add(new Image("resources/images/trainIcon.png"));
				dialog.showAndWait();
				return;
			}
		} catch (FileNotFoundException ex) {
			ticket.appendText(ex.toString());
		}

		final VBox container = new VBox(ticket);
		container.setPrefHeight(500);
		container.setSpacing(5);
		container.setAlignment(Pos.CENTER);
		container.setPadding(new Insets(10, 10, 10, 10));

		Scene ticketScene = new Scene(container);
		helpWindow.setScene(ticketScene);
		helpWindow.show();
	}

	@SuppressWarnings("resource")
	private void getHelpWindow() {

		final Stage helpWindow = new Stage();
		helpWindow.getIcons().add(new Image("resources/images/trainIcon.png"));
		helpWindow.initModality(Modality.APPLICATION_MODAL);
		helpWindow.initOwner(primaryStage);
		helpWindow.setTitle("Help");

		final TextArea info = new TextArea();
		info.setFocusTraversable(false);
		info.setEditable(false);
		info.setWrapText(true);
		info.setPrefHeight(450);

		try {
			Scanner s = new Scanner(new File("help.txt")).useDelimiter(" ");
			while (s.hasNext()) {
				if (s.hasNextInt()) {
					info.appendText(s.nextInt() + " ");
				} else {
					info.appendText(s.next() + " ");
				}
			}
		} catch (FileNotFoundException ex) {
			info.appendText(ex.toString());
		}

		final VBox container = new VBox(info);
		container.setPrefHeight(500);
		container.setSpacing(5);
		container.setAlignment(Pos.CENTER);
		container.setPadding(new Insets(10, 10, 10, 10));

		Scene helpScene = new Scene(container);
		helpWindow.setScene(helpScene);
		helpWindow.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}