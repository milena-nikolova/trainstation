package application;

public class MainStations {

	private String startStation; 			// ������� ����
	private String endStation; 				// ������ ����
	private int distance; 					// ���������� � km
	private String departureTime; 			// ��� �� �������� HH:MM
	private String arrivalTime; 			// ��� �� ���������� HH:MM
	private String duriationTime; 			// ����������� �� ���������� HH:MM
	private String trainID; 				// ����� �� �����

	public MainStations() {

	}

	public MainStations(String startStation, String endStation, int distance, String departureTime, String arrivalTime,
			String duriationTime, String trainID) {
		this.startStation = startStation;
		this.endStation = endStation;
		this.distance = distance;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
		this.duriationTime = duriationTime;
		this.trainID = trainID;
	}

	public void Print() {
		System.out.println(startStation + " " + endStation + " " + departureTime + " " + arrivalTime + " " + trainID +" "+ duriationTime);
	}

	// Accessors and Mutators
	public int getDistance() {
		return distance;
	}

	@Override
	public String toString() {
		return "MainStations [startStation=" + startStation + ", endStation=" + endStation 
				+ ", distance=" + distance + ", departureTime=" + departureTime 
				+ ", arrivalTime=" + arrivalTime + ", duriationTime=" 
				+ duriationTime + ", trainID=" + trainID + "]";
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public String getStartStation() {
		return startStation;
	}

	public void setStartStation(String startStation) {
		this.startStation = startStation;
	}

	public String getEndStation() {
		return endStation;
	}

	public void setEndStation(String endStation) {
		this.endStation = endStation;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDuriationTime() {
		return duriationTime;
	}

	public void setDuriationTime(String duriationTime) {
		this.duriationTime = duriationTime;
	}

	public String getTrainID() {
		return trainID;
	}

	public void setTrainID(String trainID) {
		this.trainID = trainID;
	}

};